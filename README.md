Scala Docker-Image
==================

To be used as basis for containers using scala

Will be published to Docker Hub for easy use.

Deployment
----------

Update the versions-file and commit

License
-------

This code is open source software licensed under the [Apache 2.0 License]("http://www.apache.org/licenses/LICENSE-2.0.html").
